from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    with Configurator(settings=settings) as config:
        config.route_prefix = 'v1'
        config.route_prefix = 'apiaks/v1'
        config.include('pyramid_jinja2')
        config.include('.routes')
        # config.include('cornice')
        config.scan()
    return config.make_wsgi_app()
