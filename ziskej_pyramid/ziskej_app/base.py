# -*- coding: utf-8 -*-

import datetime
import json
import jwt
import os.path
from pprint import pprint
from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPForbidden,
    HTTPMethodNotAllowed,
    )
import requests

from ziskej_pyramid.ziskej_app.config import (
    SECRETS_DIR,
    ZISKEJ_API_AKS_ID,
    SHARED_SECRET_ZISKEJ_APP_PYRAMID,
    ZISKEJ_APP_BASEURL,
    ZISKEJ_APP_DISABLE_VERIFY,
    )


DEBUG = False


class ZiskejApiAksBase(object):

    allowed_methods = []
    entry_point = None

    def __init__(self, request):
        self.request = request

    def __call__(self):
        if self.request.method not in self.allowed_methods:
            raise HTTPMethodNotAllowed()
        try:
            self.call_before_zapp()
            zapp_result = self.zapp()
            zapp_result = self.call_after_zapp(zapp_result)
            return zapp_result
        except Exception as e:
            try:
                http_status = int(str(self.request.response.status)[:3])
            except ValueError:
                http_status = 200
            if http_status < 500:
                self.request.response.status = 400
            else:
                self.request.response.status = 500
            return {'error_message': str(e)}

    def call_before_zapp(self):
        pass

    def call_after_zapp(self, zapp_result):
        return zapp_result

    def validation(self, data):
        pass

    def _check_jwt(self, sigla):
        request = self.request
        authorization = request.authorization
        is_authorized = False
        if not authorization:
            request.error_message_ziskej = 'Missing authorization header.'
            raise HTTPForbidden()
        authtype = authorization.authtype
        token = authorization.params
        if not authtype or authtype.lower() != 'bearer':
            request.error_message_ziskej = \
                'Invalid authorization header format.'
            raise HTTPForbidden()
        if not token:
            request.error_message_ziskej = \
                'Invalid authorization header format.'
            raise HTTPForbidden()

        try:
            alg = jwt.get_unverified_header(token).get('alg', '')
        except jwt.DecodeError as e:
            request.error_message_ziskej = \
                'Invalid authorization token format: Missing algorithm.'
            raise HTTPForbidden()
        if alg != 'HS256':
            request.error_message_ziskej = \
                'Invalid authorization token format: Invalid algorithm.'
            raise HTTPForbidden()

        app_id = '{}'.format(sigla)

        secret_filename = '{}-secret.txt'.format(sigla)
        filepath = os.path.join(SECRETS_DIR, secret_filename)
        if not os.path.isfile(filepath):
            if not os.path.isdir(SECRETS_DIR):
                request.error_message_ziskej = \
                    'Invalid authorization token: Invalid configuration.'
                raise HTTPForbidden()
            request.error_message_ziskej = \
                'Invalid authorization token: Invalid sigla.'
            raise HTTPForbidden()
        with open(filepath, 'rb') as f:
            secret = f.read()

        try:
            token_dict = jwt.decode(token, secret, algorithms=['HS256'])
        except jwt.ExpiredSignatureError as e:
            request.error_message_ziskej = 'Expired authorization token.'
            raise HTTPForbidden()
        except jwt.exceptions.InvalidTokenError as e:
            request.error_message_ziskej = 'Invalid authorization token.'
            raise HTTPForbidden()
        if not token_dict.get('iat', ''):
            request.error_message_ziskej = 'Missing iat in authorization token.'
            raise HTTPForbidden()
        if not token_dict.get('exp', ''):
            request.error_message_ziskej = 'Missing exp in authorization token.'
            raise HTTPForbidden()
        if token_dict.get('iss', '') != app_id:
            request.error_message_ziskej = 'Invalid iss in authorization token.'
            raise HTTPForbidden()
        if token_dict.get('app', '') != app_id:
            request.error_message_ziskej = 'Invalid app in authorization token.'
            raise HTTPForbidden()

        return token

    def get_conf(self):
        http_id = str(self.request.method)
        http_id = http_id.lower()
        method_id = 'http_{}'.format(http_id)
        method = getattr(self, method_id, None)
        if method is None:
            if DEBUG:
                print("Missing method {}".format(method_id))
            raise HTTPBadRequest('Invalid configuration')
        return method()

    def get_data(self, conf=None):
        if conf is None:
            conf = self.get_conf()
        data = dict()
        for key in conf['path_params']:
            value = self.request.matchdict.get(key, None)
            if value is None:
                if DEBUG:
                    print("Missing path param {}".format(key))
                raise HTTPBadRequest('Missing param {}'.format(key))
            data[key] = value
        if self.request.method in ('POST', 'PUT', ):
            params = self.request.json_body
        else:
            params = self.request.params
        for key in conf['mandatory_params']:
            value = params.get(key, None)
            if value is None:
                if DEBUG:
                    print("Missing param {}".format(key))
                raise HTTPBadRequest('Missing param {}'.format(key))
            data[key] = value
        for key in conf['optional_params']:
            value = params.get(key, None)
            if value is None:
                continue
            data[key] = value
        return data

    def get_zapp_token(self):
        now = datetime.datetime.utcnow()
        token_dict = dict(
            iss = ZISKEJ_API_AKS_ID,
            iat = now,
            exp = now + datetime.timedelta(seconds=300),
            app = ZISKEJ_API_AKS_ID,
            )
        token = jwt.encode(
            token_dict, SHARED_SECRET_ZISKEJ_APP_PYRAMID, algorithm='HS256')
        return token

    def zapp(self):
        conf = self.get_conf()
        data = self.get_data(conf=conf)
        if DEBUG:
            self.print(conf=conf, data=data)
        self.validation(data)
        post_data = data.copy()
        post_data['action_id'] = conf['action_id']
        post_data['aks_token'] = self.aks_token
        post_data['apiaks_token'] = self.get_zapp_token()
        url = ZISKEJ_APP_BASEURL
        if ZISKEJ_APP_DISABLE_VERIFY:
            result_obj = requests.post(url, data=post_data, verify=False)
        else:
            result_obj = requests.post(url, data=post_data)
        if not result_obj.ok:
            if DEBUG:
                print("Ziskej app response is not OK - is it running?")
            raise HTTPBadRequest('Ziskej app failed.')
        result = result_obj.json()  # dict
        http_code = result.get('http_code', None)
        if result['status'] == 'OK':
            if http_code:
                self.request.response.status = http_code
            else:
                self.request.response.status = 200
            # if http_code == 201:
            #     self.request.response.status = 201
            if http_code == 204:
                return None
            return result['result']
        if http_code:
            self.request.response.status = http_code
        else:
            self.request.response.status = 500
        error_message = result.get('error_message', 'Ziskej app exception.')
        if DEBUG:
            print("Ziskej app exception: {}".format(error_message))
        raise HTTPBadRequest(error_message)

    def print(self, conf=None, data=None):
        if conf is None:
            conf = self.get_conf()
        print(" ")
        print(conf['action_id'], self.request.method)
        if data is not None:
            pprint(data)
        if self.request.method in ('POST', 'PUT', ):
            print("json_body:")
            pprint(self.request.json_body)
        print(" ")

    def get_sigla_from_path_for_jwt(self):
        sigla = self.request.matchdict.get('sigla', None)
        if not sigla:
            raise HTTPBadRequest('Missing sigla.')
        if not isinstance(sigla, str):
            raise HTTPBadRequest('Invalid sigla.')
        if len(sigla) != 6:
            raise HTTPBadRequest('Invalid sigla.')
        sigla = sigla.lower()
        return sigla

class ZiskejApiAksBaseAnon(ZiskejApiAksBase):

    def __init__(self, request):
        super().__init__(request)


class ZiskejApiAksBaseAuth(ZiskejApiAksBase):

    def __init__(self, request):
        super().__init__(request)
        sigla = self.get_sigla_from_path_for_jwt()
        self.aks_token = self._check_jwt(sigla)
