# -*- coding: utf-8 -*-

import os
import urllib3


# DEFAULTS
SECRETS_DIR = '/data/apiaks-secrets'
SHARED_SECRET_ZISKEJ_APP_PYRAMID = 'FIXME'

for key, value in os.environ.items():
    if key == 'ZISKEJ_APIAKS_MODE':
        ZISKEJ_APIAKS_MODE = value
    elif key == 'SHARED_SECRET_ZISKEJ_APP_PYRAMID':
        SHARED_SECRET_ZISKEJ_APP_PYRAMID = value
    continue
if ZISKEJ_APIAKS_MODE not in ('LOCAL', 'DEV', 'TEST', ):
    raise Exception('Invalid ZISKEJ_APIAKS_MODE')

if ZISKEJ_APIAKS_MODE == 'LOCAL':
    # Local
    ZISKEJ_API_AKS_ID = 'ziskej-apiaks.LOCAL'
    ZISKEJ_APP_BASEURL = 'http://ziskej.private/apiaks'
    ZISKEJ_APP_DISABLE_VERIFY = False
elif ZISKEJ_APIAKS_MODE == 'DEV':
    # Yavanna
    ZISKEJ_API_AKS_ID = 'ziskej-apiaks.TEST'
    ZISKEJ_APP_BASEURL = 'https://ziskej-test.techlib.cz/apiaks'
    ZISKEJ_APP_DISABLE_VERIFY = True
elif ZISKEJ_APIAKS_MODE == 'TEST':
    # Z-TEST
    ZISKEJ_API_AKS_ID = 'ziskej-apiaks.TEST'
    ZISKEJ_APP_BASEURL = 'https://ziskej-test.techlib.cz/apiaks'
    ZISKEJ_APP_DISABLE_VERIFY = True

if ZISKEJ_APP_DISABLE_VERIFY:
    print(' ')
    print('SSL warnings are disabled '\
          'due to invalid certificate at techlib server.')
    print(' ')
    # https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
    urllib3.disable_warnings()

print('ZISKEJ_APIAKS_MODE:', ZISKEJ_APIAKS_MODE)
print('SHARED_SECRET_ZISKEJ_APP_PYRAMID:', SHARED_SECRET_ZISKEJ_APP_PYRAMID)
print('ZISKEJ_API_AKS_ID:', ZISKEJ_API_AKS_ID)
print('ZISKEJ_APP_BASEURL:', ZISKEJ_APP_BASEURL)
print('ZISKEJ_APP_DISABLE_VERIFY:', ZISKEJ_APP_DISABLE_VERIFY)
print(' ')
