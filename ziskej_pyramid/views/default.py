# -*- coding: utf-8 -*-

from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPFound,
    )
from pyramid.view import view_config

from ziskej_pyramid.ziskej_app.base import (
    ZiskejApiAksBaseAnon,
    ZiskejApiAksBaseAuth,
    )


@view_config(route_name='docs')
def docs_view(request):
    return HTTPFound('https://ziskej-info.techlib.cz/dokumentace/pro-spravce-knihovny/api-pro-aks')


@view_config(route_name='register_callback', renderer='json')
class RegisterCallback(ZiskejApiAksBaseAuth):
    allowed_methods = ('PUT', )
    entry_point = '/libraries/{sigla}/callbacks/{callback_id}'
    callback_ids = (
        'event_reader_ticket_changed', 'event_zk_subticket_changed',
        'event_dk_subticket_changed', )

    def validation(self, data):
        callback_id = data.get('callback_id', None)
        if callback_id not in self.callback_ids:
            raise HTTPBadRequest(
                'Invalid callback_id.')

    def http_put(self):
        return dict(
            action_id = 'register_callback',
            path_params = ['sigla', 'callback_id', ],
            mandatory_params = ['url', 'http', ],
            optional_params = [],
            )


@view_config(route_name='zk_tickets', renderer='json')
class ZkTickets(ZiskejApiAksBaseAuth):
    allowed_methods = ('GET', )
    entry_point = '/libraries/{sigla}/tickets'

    def validation(self, data):
        if data.get('eppn', None) and data.get('reader_id', None):
            raise HTTPBadRequest(
                'Only one parameter is allowed from: eppn, reader_id.')
        counter = 0
        if data.get('status', None):
            counter += 1
        if data.get('is_open', None):
            counter += 1
        if data.get('open_and_closed_in', None):
            counter += 1
        if counter > 1:
            raise HTTPBadRequest(
                'Only one parameter is allowed from: status, is_open, and '
                'open_and_closed_in.')
        if counter == 0 and data.get('expand', None):
            raise HTTPBadRequest(
                'Parameter expand is allowed only with status, is_open, or '
                'open_and_closed_in.')

    def http_get(self):
        return dict(
            action_id = 'zk_tickets',
            path_params = ['sigla', ],
            mandatory_params = [],
            optional_params = [
                'eppn', 'reader_id', 'status', 'is_open', 'open_and_closed_in',
                'expand', ],
            )


@view_config(route_name='zk_ticket', renderer='json')
class ZkTicket(ZiskejApiAksBaseAuth):
    allowed_methods = ('GET', )
    entry_point = '/libraries/{sigla}/tickets/{ticket_id}'

    def http_get(self):
        return dict(
            action_id = 'zk_ticket',
            path_params = ['sigla', 'ticket_id', ],
            mandatory_params = [],
            optional_params = [],
            )


@view_config(route_name='zk_ticket_reader_info', renderer='json')
class ZkTicketReaderInfo(ZiskejApiAksBaseAuth):
    allowed_methods = ('PUT', )
    entry_point = '/libraries/{sigla}/tickets/{ticket_id}/reader_info'

    def http_put(self):
        return dict(
            action_id = 'zk_ticket_reader_info',
            path_params = ['sigla', 'ticket_id', ],
            mandatory_params = [
                'reader_from_date', 'reader_to_date', 'reader_note',
                'reader_place', ],
            optional_params = ['is_reader_free', ],
            )


@view_config(route_name='zk_ticket_wf', renderer='json')
class ZkTicketWf(ZiskejApiAksBaseAuth):
    allowed_methods = ('POST', )
    entry_point = '/libraries/{sigla}/tickets/{ticket_id}/transitions'

    def validation(self, data):
        transition_ids = ['prepare', 'lent', 'return', 'close', ]
        if data.get('transition_id', None) not in transition_ids:
            raise HTTPBadRequest(
                'Invalid value of parameter transition_id.')

    def http_post(self):
        return dict(
            action_id = 'zk_ticket_transition',
            path_params = ['sigla', 'ticket_id', ],
            mandatory_params = ['transition_id', ],
            optional_params = ['transition_message', ],
            )


@view_config(route_name='zk_subtickets', renderer='json')
class ZkSubtickets(ZiskejApiAksBaseAuth):
    allowed_methods = ('GET', )
    entry_point = '/libraries/{sigla}/roles/zk/subtickets'

    def validation(self, data):
        counter = 0
        if data.get('status', None):
            counter += 1
        if data.get('is_open', None):
            counter += 1
        if data.get('open_and_closed_in', None):
            counter += 1
        if counter > 1:
            raise HTTPBadRequest(
                'Only one parameter is allowed from: status, is_open, and '
                'open_and_closed_in.')
        if counter == 0 and data.get('expand', None):
            raise HTTPBadRequest(
                'Parameter expand is allowed only with status, is_open, or '
                'open_and_closed_in.')
        status_ids = ['queued', 'conditionally_accepted', 'accepted', 'sent',
                      'sent_back', 'closed', ]
        if data.get('status', None) and \
                data.get('status', None) not in status_ids:
            raise HTTPBadRequest(
                'Invalid value of parameter status.')
        if data.get('expand', None) and data.get('expand', None) != 'detail':
            raise HTTPBadRequest(
                'Invalid value of parameter expand.')

    def http_get(self):
        return dict(
            action_id = 'zk_subtickets',
            path_params = ['sigla', ],
            mandatory_params = [],
            optional_params = [
                'status', 'is_open', 'open_and_closed_in', 'expand', ],
            )


@view_config(route_name='zk_subticket', renderer='json')
class ZkSubticket(ZiskejApiAksBaseAuth):
    allowed_methods = ('GET', )
    entry_point = '/libraries/{sigla}/roles/zk/subtickets/{subticket_id}'

    def http_get(self):
        return dict(
            action_id = 'zk_subticket',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = [],
            optional_params = [],
            )


@view_config(route_name='zk_subticket_wf', renderer='json')
class ZkSubticketWf(ZiskejApiAksBaseAuth):
    allowed_methods = ('POST', )
    entry_point = '/libraries/{sigla}/roles/zk/subtickets/{subticket_id}'\
                  '/transitions'

    def validation(self, data):
        transition_ids = ['send_back', ]
        if data.get('transition_id', None) not in transition_ids:
            raise HTTPBadRequest(
                'Invalid value of parameter transition_id.')

    def http_post(self):
        return dict(
            action_id = 'zk_subticket_transition',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = ['transition_id', ],
            optional_params = ['transition_message', ],
            )


@view_config(route_name='zk_subticket_pm', renderer='json')
class ZkSubticketPm(ZiskejApiAksBaseAuth):
    allowed_methods = ('GET', 'POST', )
    entry_point = '/libraries/{sigla}/roles/zk/subtickets/{subticket_id}'\
                  '/messages'

    def http_get(self):
        return dict(
            action_id = 'zk_subticket_messages_get',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = [],
            optional_params = [],
            )

    def http_post(self):
        return dict(
            action_id = 'zk_subticket_messages_post',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = ['text', ],
            optional_params = [],
            )


@view_config(route_name='zk_subticket_pm_unread', renderer='json')
class ZkSubticketPmUnread(ZiskejApiAksBaseAuth):
    allowed_methods = ('PUT', )
    entry_point = '/libraries/{sigla}/roles/zk/subtickets/{subticket_id}'\
                  '/messages/unread'

    def http_put(self):
        return dict(
            action_id = 'zk_subticket_pm_unread',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = ['unread', ],
            optional_params = [],
            )


@view_config(route_name='dk_subtickets', renderer='json')
class DkSubtickets(ZiskejApiAksBaseAuth):
    allowed_methods = ('GET', )
    entry_point = '/libraries/{sigla}/roles/dk/subtickets'

    def validation(self, data):
        counter = 0
        if data.get('status', None):
            counter += 1
        if data.get('is_open', None):
            counter += 1
        if data.get('open_and_closed_in', None):
            counter += 1
        if counter > 1:
            raise HTTPBadRequest(
                'Only one parameter is allowed from: status, is_open, and '
                'open_and_closed_in.')
        if counter == 0 and data.get('expand', None):
            raise HTTPBadRequest(
                'Parameter expand is allowed only with status, is_open, or '
                'open_and_closed_in.')

    def http_get(self):
        return dict(
            action_id = 'dk_subtickets',
            path_params = ['sigla', ],
            mandatory_params = [],
            optional_params = [
                'status', 'is_open', 'open_and_closed_in', 'expand', ],
            )


@view_config(route_name='dk_subticket', renderer='json')
class DkSubticket(ZiskejApiAksBaseAuth):
    allowed_methods = ('GET', )
    entry_point = '/libraries/{sigla}/roles/dk/subtickets/{subticket_id}'

    def http_get(self):
        return dict(
            action_id = 'dk_subticket',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = [],
            optional_params = [],
            )


@view_config(route_name='dk_subticket_dk_info', renderer='json')
class DkSubticketDkInfo(ZiskejApiAksBaseAuth):
    allowed_methods = ('PUT', )
    entry_point = '/libraries/{sigla}/roles/dk/subtickets/{subticket_id}'\
                  '/dk_info'

    def http_put(self):
        return dict(
            action_id = 'dk_subticket_dk_info',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = ['doc_dk_id', 'rfid', 'barcode', 'back_date', ],
            optional_params = ['fee', ],
            )


@view_config(route_name='dk_subticket_wf', renderer='json')
class DkSubticketWf(ZiskejApiAksBaseAuth):
    allowed_methods = ('POST', )
    entry_point = '/libraries/{sigla}/roles/dk/subtickets/{subticket_id}'\
                  '/transitions'

    def validation(self, data):
        transition_ids = ('accept', 'refuse', 'send', 'close', )
        if data.get('transition_id', None) not in transition_ids:
            raise HTTPBadRequest(
                'Invalid value of parameter transition_id.')
        if data.get('transition_id', None) == 'refuse':
            refuse_reason_ids = (
                'refuse-reason-wrong-citation', 'refuse-reason-dk-impossible',
                'refuse-reason-dk-unavailable', )
            refuse_reason = data.get('refuse_reason', None)
            if not refuse_reason:
                raise HTTPBadRequest(
                    'Missing parameter refuse_reason.')
            if refuse_reason not in refuse_reason_ids:
                raise HTTPBadRequest(
                    'Invalid value of parameter refuse_reason.')

    def http_post(self):
        return dict(
            action_id = 'dk_subticket_transition',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = ['transition_id', ],
            optional_params = ['transition_message', 'refuse_reason', ],
            )


@view_config(route_name='dk_subticket_pm', renderer='json')
class DkSubticketPm(ZiskejApiAksBaseAuth):
    allowed_methods = ('GET', 'POST', )
    entry_point = '/libraries/{sigla}/roles/dk/subtickets/{subticket_id}'\
                  '/messages'

    def http_get(self):
        return dict(
            action_id = 'dk_subticket_messages_get',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = [],
            optional_params = [],
            )

    def http_post(self):
        return dict(
            action_id = 'dk_subticket_messages_post',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = ['text', ],
            optional_params = [],
            )


@view_config(route_name='dk_subticket_pm_unread', renderer='json')
class DkSubticketPmUnread(ZiskejApiAksBaseAuth):
    allowed_methods = ('PUT', )
    entry_point = '/libraries/{sigla}/roles/dk/subtickets/{subticket_id}'\
                  '/messages/unread'

    def http_put(self):
        return dict(
            action_id = 'dk_subticket_pm_unread',
            path_params = ['sigla', 'subticket_id', ],
            mandatory_params = ['unread', ],
            optional_params = [],
            )
