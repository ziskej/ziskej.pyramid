from pyramid.view import (
    notfound_view_config,
    forbidden_view_config,
    exception_view_config,
    )


@notfound_view_config(renderer='json')
def notfound_view(request):
    request.response.status = 404
    return {'error_message': 'Not found'}

@forbidden_view_config(renderer='json')
def forbidden_view(request):
    try:
        error_message_ziskej = str(request.error_message_ziskej)
        print("403 error_message:", error_message_ziskej)
    except:
        error_message_ziskej = 'Unauthorized access'
        print("403 STD error_message")
    request.response.status = 403
    return {'error_message': error_message_ziskej}

@exception_view_config(Exception, renderer='json')
def error_view(request):
    return {'error_message': str(request.exception)}
