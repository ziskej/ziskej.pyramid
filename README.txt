Ziskej Pyramid 
===============

Installation
------------

- Change directory into your newly created project if not already there. Your
  current directory should be the same as this README.txt file and setup.py.

    cd ziskej_pyramid

- Create a Python virtual environment, if not already created.

    python3 -m venv env

- Upgrade packaging tools, if necessary.

    env/bin/pip install --upgrade pip setuptools

- Install the project in editable mode with its testing requirements.

    env/bin/pip install -e .

- Run your project.

    env/bin/pserve development.ini
    env/bin/pserve development.ini --reload
    env/bin/gunicorn --paste production.ini -c gunicorn-config.py
