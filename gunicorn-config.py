bind = 'unix:/data/pyramid/gunicorn.sock'

# pythonpath = '/data/pyramid/ziskej_pyramid/ziskej_pyramid'
max_requests = 1000
timeout = 300
preload = True
accesslog = '/data/pyramid/logs/gunicorn-access.log'
errorlog = '/data/pyramid/logs/gunicorn-error.log'
pidfile = '/data/pyramid/gunicorn.pid'

# Change to DEBUG mode and enable for debug mode, source code will be changed in realtime without restarting systemd
loglevel = 'INFO'
debug = False
reload = False

timeout = 300
# workers = 30
workers = 4

raw_env = [
    'ZISKEJ_APIAKS_MODE=FIXME',
    'SHARED_SECRET_ZISKEJ_APP_PYRAMID="FIXME"'
]
